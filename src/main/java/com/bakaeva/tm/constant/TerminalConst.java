package com.bakaeva.tm.constant;

public interface TerminalConst {

    String HELP = "help";
    String ABOUT = "about";
    String VERSION = "version";

}
